<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::all();
        return response()->json($barang);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'category' => 'required|max:255',
        ]);

        $svbarang = new Barang;
        $svbarang->name = $request->name;
        $svbarang->category = $request->category;
        if ($request->photo) {
            $file = $request->photo;
            $filename = date('YmdHi') . $file->getClientOriginalName();
            $file->move(public_path('image/'), $filename);
            $data['image'] = $filename;
            $svbarang->photo = $data['image'];
        }
        $svbarang->save();
        return response()->json([
            "status" => true,
            "message" => 'Berhasil Menambah',
            "data" => $svbarang
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $barang = Barang::find($id);
        return response()->json($barang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'category' => 'required|max:255',
        ]);
        $path = public_path('image/');
        $id = $request->id;
        $svbarang = Barang::find($id);
        $svbarang->name = $request->name;
        if ($svbarang->photo != null && file_exists($path . $svbarang->photo)) {

            unlink($path . $svbarang->photo);
        }

        $svbarang->category = $request->category;
        $file = $request->photo;
        $filename = date('YmdHi') . $file->getClientOriginalName();
        $file->move(public_path('image/'), $filename);
        $data['image'] = $filename;
        $svbarang->photo = $data['image'];
        // $svbarang->photo = $data['image'];
        $svbarang->save();
        return response()->json([
            "status" => true,
            "message" => 'Berhasil Mengedit',
            "data" => $svbarang
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);

        File::delete('image/' . $barang->photo);
        $barang->delete();

        return response()->json([
            "status" => true,
            "message" => 'Berhasil Menghapus',
            "data" => $barang
        ]);
    }
}
