window.Vue = require("vue").default;
import VueRouter from "vue-router";
Vue.use(VueRouter);

const Home = require("../pages/Home.vue").default;
const About = require("../pages/About.vue").default;
import NotFound from "../pages/NotFound.vue";
import Barang from "../pages/Barang.vue";
import DetailBarang from "../pages/DetailBarang.vue";
import TambahBarang from "../pages/TambahBarang.vue";
import EditBarang from "../pages/EditBarang.vue";

const routes = [
    {
        name: "Home",
        path: "/home",
        component: Home,
    },
    {
        name: "About",
        path: "/about",
        component: About,
    },
    {
        name: "Barang",
        path: "/barang",
        component: Barang,
    },
    {
        name: "TambahBarang",
        path: "/barang/create",
        component: TambahBarang,
    },
    {
        name: "DetailBarang",
        path: "/barang/:id",
        component: DetailBarang,
        props: true,
    },
    {
        name: "EditBarang",
        path: "/editbarang/:id",
        component: EditBarang,
        props:true
    },
    {
        path: "*",
        component: NotFound,
    },
];

const router = new VueRouter({
    linkActiveClass: "active",
    mode: "history",
    routes,
});

export default router;
