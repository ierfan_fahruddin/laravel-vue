<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel-Vue</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
        body {
            margin: 10%;
        }
    </style>
</head>

<body>
    <div id="app">
        <header-component></header-component>
        <h1></h1>
        <router-view></router-view>
        <footer-component></footer-component>
    </div>
    <script src="{{ asset('js/app.js')}}"></script>
</body>

</html>